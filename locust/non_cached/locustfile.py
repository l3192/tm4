from locust import task, FastHttpUser

class Tugas4StressTestNonCached(FastHttpUser):
    @task
    def get_data(self):
        self.client.get("/read/1234")
