from django.urls import path, include

from rest_framework.routers import SimpleRouter

from app.views import AppViewSet

router = SimpleRouter(trailing_slash=False)
router.register('', AppViewSet, basename='app-view-set')

urlpatterns = [
    path('', include(router.urls)),
]