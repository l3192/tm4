import json
from unittest.util import _MAX_LENGTH
from django.http import JsonResponse
from rest_framework import fields
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import ViewSet

from app.models import UserModel


class PostRequestSerializer(Serializer):
    npm = fields.CharField(required=True, max_length=255)
    nama = fields.CharField(required=True, max_length=255)


class AppViewSet(ViewSet):
    @action(methods=['post'], detail=False, url_path='update')
    def post_data(self, request: Request):
        serializer = PostRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        user = UserModel.objects.filter(npm=data['npm']).first()
        if user:
          user.nama = data['nama']
          user.save()
        else:
          UserModel.objects.create(npm=data['npm'], nama=data['nama'])

        return JsonResponse({'status': 'OK'})

    @action(methods=['get'], detail=False, url_path='read/(?P<npm>[\w-]+)')
    def get_data(self, request: Request, npm: str):
        user = UserModel.objects.filter(npm=npm).first()
        if not user:
          return JsonResponse({'status': f'NPM {npm} Not Found'}, status=404)

        return JsonResponse({'status': 'OK', 'npm': user.npm, 'nama': user.nama})
