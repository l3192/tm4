## TM4
- Ryo Axtonlie - 1806205571

### Requirement:
- Python 3.9.5
- Pipenv
- PostgreSQL

### To use:
1. Clone this repository
2. Change USER and PASSWORD value on `TM4/settings.py` to match your PSQL credential 
3. Create database `tm4` on your PSQL
4. Run `pipenv shell` on the cloned directory
5. Run `pipenv install` on the cloned directory
6. Run `python manage.py migrate` to migrate the database
5. Run `python manage.py runserver` to run server

### This repository contain:
- Django webserver core (on `/app` and `/TM4` directory)
- `nginx.conf` file containing configuration for reverse proxy and caching 
- Locust configuration for stress testing located on `/locust` directory